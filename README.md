# File browse app

Design an iOS app that get a file list by Websocket.
Than user can select an item to download or play.

## To iOS APP Candidate
Hi ,Thanks for your interest in the position. We have an iOS APP exam before interview. 

Please finish it and commit your code to GitLab before interview. 

You may need the permission to commit and create branch, please provide your GitLab account to us.

## Requirements

* There is a **Button** that you need to show in the first view

* Click the **Button** to do the following steps
	1. Open a websocket connection. (Websocket URL :　wss://interview.transcendcloud.com
	2. Make sure the connection is right
	3. Send a message data **{"action":"getURL"}** to get a **Filelist URL**
	
* Parse the XML that you get form Websocket.  (The xml includes **File Path**, **file name**, **File size** and **Video length**)

* Present the information of each file, including **file name**, **File size** and **Video length** in list view.

* Offer a "Download" button to download a selected video using the path that in xml.

* There are a **Progress Bar** and a **Label** that need to show during downloading. Both of the **Progress Bar** and the **Label** show the percentage of downloaded file. (0% to 100%)



## Note

* You can't import or use any third-party **framework / library**.


![](https://s3.ap-northeast-1.amazonaws.com/test.storejetcloud.com/CandidateFiles/image.jpg)

