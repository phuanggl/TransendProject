//
//  ViewController.swift
//  TranscendProject
//
//  Created by Penny Huang on 2021/9/13.
//

import UIKit
import Photos

class ViewController: UIViewController {

    private var model = FileListModel()
    private var temp = FileModel()
    private var foundCharacters = ""
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(willResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    @IBAction func getFileButtonPressed(_ sender: Any) {
        WebSocketService.shared.connect()

        WebSocketService.shared.receive() { url in
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, let _ = response else { return }
                let xml = XMLParser(data: data)
                xml.delegate = self
                xml.parse()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }.resume()
        }
    }
    
    @objc func willResignActive() {
        WebSocketService.shared.close()
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model.FileCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FileTableViewCell", for: indexPath) as? FileTableViewCell else { return UITableViewCell()}
        cell.delegate = self
        
        let file = model.FileList[indexPath.row]
        cell.setCell(with: file)
        
        return cell
    }
}

// MARK: - FileTableViewCellDelegate
extension ViewController: FileTableViewCellDelegate {
    func downloadFile(from path: String) {
        print("path: \(path)")
        guard let url = URL(string: path) else { return }
        URLWebService.downloadVideo(from: url, delegate: self)
    }
}

// MARK: - URLSession Delegate
extension ViewController: URLSessionDownloadDelegate, URLSessionDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print(location)
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        DispatchQueue.main.async {
            self.progressLabel.text = "\(Int(progress * 100))%"
            self.progressView.progress = progress
        }
    }
}

// MARK: - XMLParser
extension ViewController: XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        foundCharacters = string
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        switch elementName {
        case "FileCount":
            model.FileCount = Int(foundCharacters) ?? 0
        case "FileName":
            temp = FileModel()
            temp.FileName = foundCharacters
        case "FilePath":
            temp.FilePath = foundCharacters
        case "FileSize":
            temp.FileSize = Int(foundCharacters) ?? 0
        case "FileDuration":
            temp.FileDuration = Int(foundCharacters) ?? 0
            model.FileList.append(temp)
        default:
            break
        }
        
        foundCharacters = ""
    }
}
