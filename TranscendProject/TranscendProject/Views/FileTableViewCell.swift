//
//  FileTableViewCell.swift
//  TranscendProject
//
//  Created by Penny Huang on 2021/9/16.
//

import UIKit

protocol FileTableViewCellDelegate: AnyObject {
    func downloadFile(from path: String)
}

class FileTableViewCell: UITableViewCell {

    @IBOutlet var fileNameLabel: UILabel!
    @IBOutlet var sizeLabel: UILabel!
    @IBOutlet var lengthLabel: UILabel!

    var file = FileModel()
    weak var delegate: FileTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setCell(with fileModel: FileModel) {
        file = fileModel
        fileNameLabel.text = fileModel.FileName
        sizeLabel.text = "Size: " + formatSize(fileModel.FileSize)
        lengthLabel.text = "Duration: " + formatDuration(fileModel.FileDuration)
    }
    
    
    func formatDuration(_ second: Int) -> String {
        let hour = String(second / (60 * 60))
        let min = String(second / 60)
        let sec = String(second % 60)
        return "\(hour == "0" ? "00" : hour):\(min.count == 1 ? "0\(min)" : min):\(sec == "0" ? "00" : sec)"
    }
    
    func formatSize(_ size: Int) -> String {
        if size > 1024 * 1024 {
            let doubleSize = Double(size) / (1024.0 * 1024.0)
            return String(format: "%.2f", doubleSize) + "GB"
        } else if size > 1024 {
            let doubleSize = Double(size) / 1024.0
            return String(format: "%.2f", doubleSize) + "MB"
        } else {
            return "\(size)KB"
        }
    }
    
    @IBAction func downloadButtonPressed(_ sender: Any) {
        delegate?.downloadFile(from: file.FilePath)
    }
}
