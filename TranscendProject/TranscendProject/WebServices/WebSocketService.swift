//
//  WebSocketService.swift
//  TranscendProject
//
//  Created by Penny Huang on 2021/9/15.
//

import Foundation

class WebSocketService {
    static let shared = WebSocketService()
    private var webSocketTask: URLSessionWebSocketTask!
    
    private init() {
        let url = URL(string: "wss://interview.transcendcloud.com")
        webSocketTask = URLSession.shared.webSocketTask(with: url!)
    }
    
    func connect() {
        webSocketTask.resume()
        ping()
        
        let message = URLSessionWebSocketTask.Message.string("{\"action\":\"getURL\"}")
        webSocketTask.send(message) { error in
            if let error = error {
                print("WebSocket sending error: \(error)")
            }
        }
    }
    
    func close() {
        print("WebSocket closed")
        let reason = "Closing connection".data(using: .utf8)
        webSocketTask.cancel(with: .goingAway, reason: reason)
    }
    
    func receive(completion: @escaping(URL)->()) {
        webSocketTask.receive { result in
            switch result {
            case .failure(let error):
                print("Failed to receive message: \(error)")
            case .success(let message):
                switch message {
                case .string(let text):
                    print("Received text message: \(text)")
                    let data = Data(text.utf8)
                    guard let urlModel = try? JSONDecoder().decode(SocketDataModel.self, from: data) else { return }
                    let url = urlModel.url
                    return completion(url)

                    
                case .data(let data):
                    print("Received binary message: \(data)")
                @unknown default:
                    fatalError()
                }
            }
        }
    }
    
    func ping() {
        webSocketTask.sendPing { error in
            if let error = error {
              print("Error when sending PING \(error)")
            } else {
                print("Web Socket connection is alive")
                DispatchQueue.global().asyncAfter(deadline: .now() + 5) {
                    self.ping()
                }
            }
        }
    }
}
