//
//  URLWebService.swift
//  TranscendProject
//
//  Created by Penny Huang on 2021/9/21.
//

import Foundation

class URLWebService {
    private init() {}
    
    static func downloadVideo(from url: URL, delegate: URLSessionDelegate) {
        let config = URLSessionConfiguration.background(withIdentifier: "downloadVideo")
        let session = URLSession(configuration: config, delegate: delegate, delegateQueue: nil)
        let downloadTask = session.downloadTask(with: url)
        downloadTask.resume()
    }
}
