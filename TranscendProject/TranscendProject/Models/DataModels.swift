//
//  DataModels.swift
//  TranscendProject
//
//  Created by Penny Huang on 2021/9/15.
//

import Foundation

struct SocketDataModel: Decodable {
    let url: URL
}

struct FileListModel {
    var FileCount = 0
    var FileList = [FileModel]()
}

struct FileModel {
    var FileName = ""
    var FilePath = ""
    var FileSize = 0
    var FileDuration = 0
}
